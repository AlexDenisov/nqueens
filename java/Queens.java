import java.util.Iterator;
import java.util.LinkedList;

class Queens {
  class Position {
    byte horizontal = 0;
    byte vertical = 0;
    Position(byte h, byte v) {
      this.horizontal = h;
      this.vertical = v;
    }
  }

  private byte size;
  private short rows = 0;
  private short columns = 0;
  private int ascendingDiagonals = 0;
  private int descendingDiagonals = 0;
  private LinkedList<byte []> solutions = new LinkedList<byte []>();
  private LinkedList<Position> trace = new LinkedList<Position>();

  public Queens(byte size) throws Exception {
    if (size > 16) {
      throw new Exception("The value is too big");
    }
    this.size = size;
  }

  public void findSolutions() {
    byte begin = 0;
    Position startingPosition = new Position(begin, begin);
    findSolutions(startingPosition);
  }

  public void printSolutions(boolean verbose) {
    System.out.println(size + " -> " + solutions.size());
    if (!verbose) {
      return;
    }
    for (byte[] solution: solutions) {
      System.out.print("{ ");
      for (byte position: solution) {
        System.out.print(position + " ");
      }
      System.out.println("}");
    }
  }

  private void findSolutions(Position position) {
    if (position.vertical > size || position.horizontal > size) {
      removeLastPlacedQueen();
      return;
    }

    if (!canPlaceQueen(position)) {
      Position moveDown = new Position(position.horizontal, (byte)(position.vertical + 1));
      findSolutions(moveDown);
      return;
    }

    placeQueen(position);

    Position moveRight = new Position((byte)(position.horizontal + 1), (byte)0);
    findSolutions(moveRight);

    Position moveDown = new Position(position.horizontal, (byte)(position.vertical + 1));
    findSolutions(moveDown);
  }

  private void placeQueen(Position queen) {
    trace.add(queen);
    flipQueen(queen);

    if (queen.horizontal == size - 1) {
      byte[] solution = new byte[size];
      Iterator<Position> li = trace.descendingIterator();
      int i = 0;
      while (li.hasNext()) {
        solution[i++] = li.next().vertical;
      }
      solutions.add(solution);
    }
  }

  private boolean canPlaceQueen(Position position) {
    if (position.horizontal >= size || position.vertical >= size) {
      return false;
    }

    boolean horizontal = (rows & (1 << position.horizontal)) != 0;
    boolean vertical = (columns & (1 << position.vertical)) != 0;
    boolean ascendingDiagonal = (ascendingDiagonals & (1 << (size - position.vertical + position.horizontal - 1))) != 0;
    boolean descendingDiagonal = (descendingDiagonals & (1 << (position.vertical + position.horizontal))) != 0;

    if (horizontal || vertical || ascendingDiagonal || descendingDiagonal) {
      return false;
    }

    return true;
  }

  private void removeLastPlacedQueen() {
    if (trace.size() == 0) {
      return;
    }

    Position queen = trace.removeLast();
    flipQueen(queen);
  }

  private void flipQueen(Position queen) {
    rows ^= (1 << queen.horizontal);
    columns ^= (1 << queen.vertical);
    ascendingDiagonals ^= (1 << (size - queen.vertical + queen.horizontal - 1));
    descendingDiagonals ^= (1 << (queen.vertical + queen.horizontal));
  }

  public static void main(String args[]) throws Exception {
    for (byte size = 0; size <= 16; size++) {
      Queens q = new Queens(size);
      q.findSolutions();
      q.printSolutions(false);
    }
  }
}

