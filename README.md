# N Queens Solution

The repository contains a solution of the [N Queens Problem](https://en.wikipedia.org/wiki/Eight_queens_puzzle).

Run the following commands to execute the Java imlpementation:

```
cd java
make
```

It will compile the program and run the algorithm against 16 boards, from 0x0
to 16x16. By default it prints only total amount of solutions, without printing
the solutions themselves (they are quite lengthy). It can be adjusted by
calling `printSolutions(true)`.

## Implementation details

To solve this problem one has to address several problems:

 - How to represent the board in memory?
 - How to check whether a queen is under attack or not?
 - How to generate all the possible positions on a board?
 - How to avoid redundant work?

All the decisions related to these issues have a direct impact on performance
and implementation. Here is my take on it.

### Naive approach

The naive implementation is to store the board as a two-dimensional array, then
generate all the possible positions of N queens on a board. The last step is
easy: go over each permutation and check whether it is good or not.

The solution does make sense, but gut feeling says it is not optimal.

### The algorithm

Instead of using an array (1D or 2D) it is better to store information about
rows, columns, and both diagonals (ascending and descending). When we place a
queen into some position `(row, column)`, then we can easily mark the
row, the column and corresponding diagonals as being attacked. Any attempt to
place another queen to a row, column, or any diagonal under attack should fail.
This way our system is always in consistent state (i.e. no two queens attack
each other). This gives the solution: if we were able to place all N queens,
then this position is correct.

For example, for a board of size N we could have the following data structure:

```
bool rows[N];
bool columns[N];
bool ascendingDiagonals[N];
bool descendingDiagonals[N];
```

By default each value is `false`, meaning that a component is not attacked.
Given a position `(row, column)` we can get the actual values using these
'formulae':

```
// Size
int N;

// Position
int row, column;

bool rowAttacked = rows[row];
bool columnAttacked = columns[column];
bool ascendingDiagonalAttacked = ascendingDiagonals[ N - column - row - 1 ];
bool descendingDiagonalAttacked = descendingDiagonals[ row + column ];
```

Using this indexing model we can easily place or remove a queen.


Instead of generating all the possible positions of queens on a board we can
try to put queens one by one and see if this disposition correct or not. If we
reach a state where solution is not available, then we must reconsider the last
placement and try another combination.

We can do that recursively:

```
void findSolutions(Position p) {
  if (outOfBoard(p)) {
    removeLastPlaceQueen();
    return;
  }

  // Shortcut
  if (!canPlaceQueen(p)) {
    findSolutions(moveDown(p));
    return;
  }

  placeQueen(p);

  findSolutions(moveRight(p));
  findSolutions(moveDown(p));
}
```

Eventually each recursive call will hit a wall (`outOfBoard`) and the whole
process will halt.

The `placeQueen` tracks the `trace` of all placed queens. Also, it records
solutions as soon as they are found.

The `trace` is simply a stack-like data structure. When we place a queen on
a board, we put this queen into the `trace`, when we remove the queen from the
board - we remove the queen from the top of the `trace`. When we put the Nth
queen, then we we copy the trace into a list of solutions.

### Notes

 - Initially I used a tree to store the trace, so that each and every
placement was recorded and available until the end of execution. When I run
this program on a 16x16 board it consumed ~60Gb of memory and crashed. With
a simple stack-like trace I could run 17x17 board and it still shows the
result.

 - I started implementing the solution in C++ because I wanted to fully focus on
the problem itself, rather than fighting unknowns of Java. I decided to
preserve both implementations in case you want to see commit history. Also,
I wnat to play a bit with my C++ implemetnation and make it faster: currently
it computes solutions for 17x17 in ~24 minutes and for 16x16 in ~4 minutes. It could be better.

 - For the Java implementation I used the very basic setup: a source file and
a Makefile. This is how I usually start working in unknown or less known
areas: use the simplest infrastructure and grow it over time if needed.

 - To save some space I decided to use bytes and shorts instead of arrays of
booleans. Because of that I had to add artifical restriction: the maximum allowed
board size is 16: one byte for rows, one byte for columns, and one short for
each diagonal. One could easily to drop this restriction by switching to short
and integers correspondingly.

 - I did not write tests. I use automated testing on a daily basis, but for
this project I decided not to use. Just as an experiment, to see how would it
work.

 - When I read about the problem on Wikipedia I first thought that there must
be some well known nice algorithm. So I went to Arxiv, skimmed through several
papers, but did not find anything I could use easily. Though, several papers
were very interesting:

   - [Isomorphism and the N Queens Problem]
(https://ir.library.oregonstate.edu/xmlui/bitstream/handle/1957/28758/Isomorphism%20and%20the%20N%20Queens%20problem.pdf)

   - [A Novel Approach to Solving N-Queens Problem]
(http://www.geocities.ws/certificatessayed/publications/sci_nqueens.pdf)

   - [A Polynomial Time Algorithm for the N Queens Problem]
(http://www.fizyka.umk.pl/~milosz/AlgIILab/10.1.1.57.4685.pdf)

