#include <iostream>
#include <assert.h>
#include <stack>
#include <list>
#include <vector>

using namespace std;

struct Position {
  uint8_t horizontal;
  uint8_t vertical;
  Position() : horizontal(0), vertical(0) {}
  Position(uint8_t r, uint8_t c) : horizontal(r), vertical(c) {}
};

class Field {
public:
  Field(int size) : size(size), rows(0), columns(0),
  ascendingDiagonals(0), descendingDiagonals(0)
  {
    if (size > 16) {
      throw new exception();
    }
  }

  bool canPlaceQueen(const Position &position) {
    if (position.horizontal >= size || position.vertical >= size) {
      return false;
    }

    bool horizontal = rows & (1 << position.horizontal);
    bool vertical = columns & (1 << position.vertical);
    bool ascendingDiagonal = ascendingDiagonals & (1 << (size - position.vertical + position.horizontal - 1));
    bool descendingDiagonal = descendingDiagonals & (1 << (position.vertical + position.horizontal));

    if (horizontal || vertical || ascendingDiagonal || descendingDiagonal) {
      return false;
    }

    return true;
  }

  void placeQueen(const Position &position) {
    validateInput(position);
    assert(canPlaceQueen(position));

    trace.push_back(position);
    flipQueen(position);

    if (position.horizontal == size - 1) {
      vector<uint8_t> solution;
      solution.reserve(size);
      for (auto it = trace.rbegin(); it != trace.rend(); it++) {
        solution.push_back(it->vertical);
      }
      solutions.push_back(solution);
    }
  }

  void removeLastPlacedQueen() {
    if (trace.empty()) {
      return;
    }
    Position lastPlaceQueen = trace.back();
    flipQueen(lastPlaceQueen);
    trace.pop_back();
  }

  void findSolutions(const Position &position) {
    if (position.horizontal > size || position.vertical > size) {
      removeLastPlacedQueen();
      return;
    }

    if (!canPlaceQueen(position)) {
      Position moveDown(position.horizontal, position.vertical + 1);
      findSolutions(moveDown);
      return;
    }

    placeQueen(position);

    Position moveRight(position.horizontal + 1, 0);
    findSolutions(moveRight);

    Position moveDown(position.horizontal, position.vertical + 1);
    findSolutions(moveDown);
  }

  void dumpSolutions() {
    printf("%d -> %lu\n", size, solutions.size());
    for (auto &solution: solutions) {
      printf(" { ");
      for (uint8_t position: solution) {
        printf("%d ", position);
      }
      printf("}\n");
    }
  }

private:
  uint8_t size;
  uint16_t rows;
  uint16_t columns;
  uint32_t ascendingDiagonals;
  uint32_t descendingDiagonals;

  list<Position> trace;
  list<vector<uint8_t>> solutions;

  void flipQueen(const Position &position) {
    rows ^= (1 << position.horizontal);
    columns ^= (1 << position.vertical);
    ascendingDiagonals ^= (1 << (size - position.vertical + position.horizontal - 1));
    descendingDiagonals ^= (1 << (position.vertical + position.horizontal));
  }

  void validateInput(const Position &position) {
    assert(position.horizontal >= 0);
    assert(position.vertical >= 0);
    assert(position.horizontal < size);
    assert(position.vertical < size);
  }
};

int main(int argc, const char * argv[]) {
  std::chrono::time_point<std::chrono::system_clock> start;
  std::chrono::time_point<std::chrono::system_clock> end;

  for (int i = 1; i <= 8; i++) {
    Field field(i);

    start = std::chrono::system_clock::now();

    field.findSolutions(Position());
    field.dumpSolutions();

    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed = end - start;
    printf("time: %lld ns\n", std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count());
    printf("time: %lld ms\n", std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count());
    printf("time: %lld s\n", std::chrono::duration_cast<std::chrono::seconds>(elapsed).count());
  }

  return 0;
}
